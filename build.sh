#!/bin/bash

function build {
	export VENDOR=$1
	FILENAME="iziit-${npm_package_version}-${VENDOR}.zip"
	if [ -f "build/${FILENAME}" ]; then
		rm -f "build/${FILENAME}"
	fi
	node_modules/parcel/bin/cli.js build src/manifest.json --no-source-maps --out-dir build-tmp
	cp COPYING build-tmp
	cd build-tmp
	zip -r "../build/${FILENAME}" *
	cd ..
}

function main {
	if [ ! -d build ]; then
		mkdir build
	fi
	build chrome
	build firefox
	rm -rf build-tmp
}

main
