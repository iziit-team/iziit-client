/*
Copyright 2018, 2019, 2020 Clément Saccoccio
Licenced under GPL-3.0-or-later

This file is part of Iziit.
Iziit is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
Iziit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Iziit. If not, see <https://www.gnu.org/licenses/>.

You can contact us at contact@iziit.org
*/

import 'regenerator-runtime/runtime';
import greenOil from '../content-scripts/scripts/common'

/*

Hey look ! Don't panic, take a map.

popup {

	main()

	utils {

		debug {
			log()
			clearLogs()
			buildPopupMap()
			objectToString(object, identSize)
		}

		colorParser {
			buildRGBArray(color)
			fracToRgb(frac)
			rgbToFrac(rgb)
			getMostContrastedColor(rgb)
		}
	}

	slidersCSSManager {
		init()
		sessionIsAboutToLoad()
	}

	panelsController {
		init()
		showPanel(panel)
		nextFrame()
		updatePanelsVisibility()
	}

	storageController {
		init()
		sessionRemovedFromStorage(sessionIndex)
		saveInputState(inputType, input)
		loadInputStates(callback)
		loadSession(sessionIndex, preventGlobalStorageUpdate, callback)
	}

	sessionsPanelUI {
		init(currentSessionIndex)
		doesSessionNameExist(name, excludedSession)
		addNewSession()
		editingStarted()
		editingTerminated()
		sessionSuccessfulyCreated(session)
		sessionDeleted(session)
		sessionEdited(session)
		loadSession(session)
		Session(name, color, select, isAnOldSession)
	}
}

*/

const popup = {

	main: function() {
		popup.slidersCSSManager.init();
		popup.panelsController.init();
		popup.storageController.init();
		// popup.sessionsPanelUI.init() is automaticaly called afer popup.storageController.init()
	},

	utils: {

		debug: {

			logs: null,

			log: function(/* ... */) {

				if (!this.logs) {
					logs = document.getElementById('logs');
					logs.style.display = 'block';
				}

				let text = '';
				for (const arg of arguments) text += arg + ' ';

				logs.textContent += text;
			},

			clearLogs: function() {
				logs.innerHTML = '';
			},

			// return a map of this code
			buildPopupMap: function() {
				return 'popup {\n\n' + this.objectToString(popup, 1) + '\n}';
			},

			// recursive function used by buildPopupMap() that retrieves a string representing an object
			objectToString: function(object, identSize) {

				if (!identSize) identSize = 0;

				let str = '';
				let ident = '';
				let line;
				let firstLoopIteration = true;

				for (let i = 0; i < identSize; i++) {
					ident += '\t';
				}

				for (let field in object) {

					line = null;

					switch (typeof object[field]) {

						case 'function':
							/( \(.*\))/.exec(object[field].toString());
							line = field + RegExp.$1;
							break;

						case 'object':
							if (!object[field] || object[field].toString().match(/^\[object (?:HTML)|(?:CSS).*\]$/)) break;

							let recursiveBlackMagic = this.objectToString(object[field], identSize + 1);

							if (recursiveBlackMagic === '') break;
							else line = field + ` {\n\n${recursiveBlackMagic}\n${ident}}`;

							if (!firstLoopIteration) line = '\n' + ident + line;

							break;
					}

					if (line) {
						if (str !== '') str += '\n';
						str += ident + line;
					}

					if (firstLoopIteration) firstLoopIteration = false;
				}

				return str;
			}
		},

		colorParser: {

			rbgPattern: /^rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)$/,

			// 'rgb(r, g, b)' => [r, g, b]
			buildRGBArray: function(color) {
				if (this.rbgPattern.exec(color)) {
					return [
						parseInt(RegExp.$1),
						parseInt(RegExp.$2),
						parseInt(RegExp.$3),
					]
				}
			},

			// frac (with 0 < frac < 1) => 'rgb(r, g, b)'
			fracToRgb: function(frac) {

				let phase = Math.floor(frac * 6);
				if (phase === 6) phase = 5;

				let increase = Math.round(frac*1530 - phase*255);
				let decrease = Math.round(255 - increase);
				let r, g, b;

				switch (phase) {

					case 0:
						r = 255;
						g = increase;
						b = 0;
						break;

					case 1:
						r = decrease;
						g = 255;
						b = 0;
						break;

					case 2:
						r = 0;
						g = 255;
						b = increase;
						break;

					case 3:
						r = 0;
						g = decrease;
						b = 255;
						break;

					case 4:
						r = increase;
						g = 0;
						b = 255;
						break;

					case 5:
						r = 255;
						g = 0;
						b = decrease;
						break;

					default:
						r = 255;
						g = 255;
						b = 255;
						break;
				}

				return `rgb(${r}, ${g}, ${b})`;
			},

			// 'rgb(r, g, b)' => frac (with 0 < frac < 1)
			rgbToFrac: function(rgb) {

				rgb = this.buildRGBArray(rgb);

				if (!rgb) return;

				let r = rgb[0];
				let g = rgb[1];
				let b = rgb[2];
				let phase;
				let increase;

				if (r === 255 && b === 0) phase = 0;
				else if (g === 255 && b === 0) phase = 1;
				else if (g === 255 && r === 0) phase = 2;
				else if (b === 255 && r === 0) phase = 3;
				else if (b === 255 && g === 0) phase = 4;
				else if (r === 255 && g === 0) phase = 5;
				else return;

				switch (phase) {
					case 0: increase = g;      break;
					case 1: increase = 255-r;  break;
					case 2: increase = b;      break;
					case 3: increase = 255-g;  break;
					case 4: increase = r;      break;
					case 5: increase = 255-b;  break;
				}

				return (increase + phase * 255) / 1530;
			},

			// 'rgb(r, g, b)' => 0 (black) or 100 (white)
			getMostContrastedColor: function(rgb) {

				rgb = this.buildRGBArray(rgb);

				// color channels weight are determinated according to the eye sensibility
				let greyScale = rgb[0] * 0.299 + rgb[1] * 0.587 + rgb[2] * 0.114;

				return greyScale > 127 ? 0 : 100;
			}
		},
	},

	// makes sliders slide smouthly when user use them or instantly when program set theire position
	slidersCSSManager: {

		sliders: document.getElementsByClassName('slider'),
		nbOfSliders: NaN,

		init: function() {

			this.nbOfSliders = this.sliders.length;

			for (let i = 0; i < this.nbOfSliders; i++) {
				this.sliders[i].addEventListener('click', () => {
					// allows slider to slide progressively when user click it
					this.sliders[i].classList.remove('instant');
				});
			}
		},

		sessionIsAboutToLoad: function() {
			for (let i = 0; i < this.nbOfSliders; i++) {
				// allows slider to slide instantly when program set its position
				this.sliders[i].classList.add('instant');
			}
		},
	},

	panelsController: {

		panels: document.querySelectorAll('.panel'),
		panelWidth: NaN,
		frameRate: 60,
		frameLength: NaN,
		defaultSpeed: NaN,
		smoothness: 0.85,
		offsetGoal: 0,
		panelGoal: 0,
		currentOffset: 0,
		currentPanel: 0,
		scrolling: false,
		timerID: null,

		init: function() {

			// set NaN variables

			this.panelWidth = /^(\d*)px$/.exec(window.getComputedStyle(this.panels[0]).width)[1];
			this.frameLength = Math.round(1000 / this.frameRate);
			this.defaultSpeed = Math.round(1500 / this.frameRate); // 1500 px/s

			// add event listeners

			let menuButtons = document.querySelectorAll('.menu-element');

			menuButtons.forEach((btn, i) => {
				btn.addEventListener('click', () => this.showPanel(i));
			});
		},

		showPanel: function(panel) {

			if (panel === this.panelGoal) return;

			this.panelGoal = panel;
			this.offsetGoal = -this.panelWidth * panel;

			if (!this.scrolling) {
				this.scrolling = true;
				this.timerID = setInterval(() => this.nextFrame(), this.frameLength);
			}

			this.updatePanelsVisibility();
		},

		nextFrame: function() {

			if (this.currentOffset === this.offsetGoal) {

				clearInterval(this.timerID);

				this.scrolling = false;
				this.currentPanel = this.panelGoal;
				this.updatePanelsVisibility();

				return;
			}

			let remaining = Math.abs(this.offsetGoal - this.currentOffset);

			let speed = Math.round(this.defaultSpeed * ((1 - this.smoothness + this.smoothness * remaining / this.panelWidth)));
			if (speed > remaining) speed = remaining;
			else if (speed === 0) speed = 1; // avoid panel animation to freeze

			if (this.currentOffset < this.offsetGoal) this.currentOffset += speed;
			else this.currentOffset -= speed;

			this.panels.forEach((panel, i) => {
				panel.style.left = this.currentOffset + this.panelWidth * i + 'px';
			});
		},

		// show panels that are or will be in the user field of view, hide the others
		updatePanelsVisibility: function() {

			if (this.scrolling) {

				// show panels, before scrolling, that will be in the user field of view according to the scroll direction

				if (this.panelGoal > this.currentPanel) {
					this.panels.forEach((panel, i) => {
						if (i >= this.currentPanel && i <= this.panelGoal) {
							panel.style.display = 'block';
						}
					});
				} else {
					this.panels.forEach((panel, i) => {
						if (i <= this.currentPanel && i >= this.panelGoal) {
							panel.style.display = 'block';
						}
					});
				}

			} else {

				// hide non visible panels after scrolling

				this.panels.forEach((panel, i) => {
					if (i !== this.currentPanel) {
						panel.style.display = 'none';
					}
				});
			}
		},
	},

	storageController: {

		preferenceButtons: document.querySelectorAll('.preference'),
		nbOfPreferenceButtons: NaN,
		currentSessionIndex: NaN,

		init: function() {

			this.nbOfPreferenceButtons = this.preferenceButtons.length;

			// add event listeners

			for (const btn of this.preferenceButtons) {
				btn.addEventListener('click', () => this.saveInputState('preferences', btn));
			}

			greenOil.storage.getAGlobalValue('currentSessionIndex', (sessionIndex) => {
				// load last used session
				this.loadSession(sessionIndex, true,
					// then init the UI
					() => popup.sessionsPanelUI.init(sessionIndex));
			}, 0);
		},

		// after called 'greenOil.storage.setSessions' this function must be called if a session was removed
		// 'sessionIndex' indicates wich session
		sessionRemovedFromStorage: function(sessionIndex) {

			// if the deleted session has an index inferior at the currently used one, the current used session index must be shifted
			if (sessionIndex < this.currentSessionIndex) {
				this.currentSessionIndex--;
				greenOil.storage.setAGlobalValue('currentSessionIndex', this.currentSessionIndex);
			}
		},

		// save a checkbox input state in the session
		saveInputState: function(inputType, input) {

			chrome.storage.local.get('sessions', (result) => {

				let sessions = result.sessions;

				// create the sessions array if it doesn't exist and add a default session in it
				if (!sessions) sessions = [{ name: 'default' }];

				let session = sessions[this.currentSessionIndex];

				// a collection is an object contained in a 'session' object that store input states
				// there is 2 collection: 'settings' and 'preferences' (cf. 'storage architecture' above)
				let collection = session[inputType];

				// create the collection object if it doesn't exist
				if (!collection) {
					collection = {};
					session[inputType] = collection;
				}

				// save the input state
				collection[input.name] = input.checked;
				chrome.storage.local.set({ sessions: sessions });
			});
		},

		// set the state of the checkbox inputs according to the stored data
		loadInputStates: function(callback) {

			chrome.storage.local.get('sessions', (result) => {

				let sessions = result.sessions;
				let storageModified = false;

				// create the sessions array if it doesn't exist and add a default session in it
				if (!sessions) {
					sessions = [{name: 'default'}];
					storageModified = true;
				}

				let session = sessions[this.currentSessionIndex];
				let preferences = session.preferences;

				// create the preference object if it doesn't exist
				if (!preferences) {
					preferences = {};
					session.preferences = preferences;
					storageModified = true;
				}

				// set the input states

				for (const input of this.preferenceButtons) {
					input.checked = typeof preferences[input.name] === 'undefined' || preferences[input.name]; // set the state to true by default (if it's not defined)
				}

				let settings = session.settings;

				// create the settings object if it doesn't exist
				if (!settings) {
					settings = {};
					session.settings = settings;
					storageModified = true;
				}

				// there is no settings for now

				// save modification
				if (storageModified) chrome.storage.local.set({ sessions: sessions });

				// imform via a callback that the input states loading is terminated
				if (callback) callback();
			});
		},

		// loads a session according to the given index
		// 'preventGlobalStorageUpdate' must be set to true if an update of 'global.currentSessionIndex' (cf. 'storage architecture') is useless
		// the callback function will be called after the session has loaded
		loadSession: function(sessionIndex, preventGlobalStorageUpdate, callback) {

			if (this.currentSessionIndex === sessionIndex) return;

			if (!preventGlobalStorageUpdate) greenOil.storage.setAGlobalValue('currentSessionIndex', sessionIndex);
			this.currentSessionIndex = sessionIndex;

			popup.slidersCSSManager.sessionIsAboutToLoad();
			this.loadInputStates(callback);
		}
	},

	sessionsPanelUI: {

		sessions: [],
		editing: false,
		panel: document.querySelector('#p2 .inner-panel'),
		maxNumberOfSessions: 8,
		addButton: document.getElementById('add'),

		init: async function(currentSessionIndex) {

			this.Session.TEMPLATE = document.getElementById('session-template').cloneNode(true);
			this.Session.TEMPLATE.id = '';

			// set other static variables

			this.Session.defaultColor = 'rgb(255, 200, 0)';

			// fill this.sessions

			greenOil.storage.getSessions((sessions) => {
				sessions.forEach((session, i) => {
					this.sessions.push(new this.Session(session.name, session.color, i === currentSessionIndex, true));
				});
				// hide the add button if the max number of sessions is reached
				this.updateAddButtonVisibility(false);
			});

			// add event listener
			this.addButton.addEventListener('click', () => this.addNewSession());
		},

		updateAddButtonVisibility: function(currentlyAddingASession) {

			let nbOfSessions = this.sessions.length;
			if (currentlyAddingASession) nbOfSessions++;

			const hidden = this.addButton.classList.contains('hidden');
			const mustBeHidden = nbOfSessions === this.maxNumberOfSessions;

			if (!hidden && mustBeHidden) this.addButton.classList.add('hidden');
			else if (hidden && !mustBeHidden) this.addButton.classList.remove('hidden');
		},

		addNewSession: function() {
			// a session can't be added while another is edited
			if (!this.editing) new this.Session();
		},

		// checks if a session name already exist the 'excludedSession' will not be checked
		doesSessionNameExist: function(name, excludedSession) {
			for (const session of this.sessions) {
				if (session !== excludedSession && session.name === name) return true;
			}

			return false;
		},

		editingStarted: function() {
			this.panel.parentNode.classList.add('prevent-non-editing-inputs');
			this.editing = true;
		},

		editingTerminated: function() {
			this.panel.parentElement.classList.remove('prevent-non-editing-inputs');
			this.editing = false;
			this.updateAddButtonVisibility(false);
		},

		sessionSuccessfulyCreated: function(session) {

			// add session to array
			this.sessions.push(session);

			greenOil.storage.getSessions((sessions) => {
				// add session to storage
				sessions.push({ name: session.name, color: session.colorPicker.pastilleColor });
				greenOil.storage.setSessions(sessions);
			});
		},

		sessionDeleted: function(session) {

			// remove session from array

			let index = this.sessions.indexOf(session);
			this.sessions.splice(index, 1);

			greenOil.storage.getSessions((sessions) => {
				// remove session from storage
				sessions.splice(index, 1);
				greenOil.storage.setSessions(sessions);
			});

			// show the add button if the number of sessions has passed under the max
			this.updateAddButtonVisibility(false);

			// notify storageController
			popup.storageController.sessionRemovedFromStorage(index);
		},

		sessionEdited: function(session) {

			let index = this.sessions.indexOf(session);

			greenOil.storage.getSessions((sessions) => {
				// update storage
				sessions[index].name = session.name;
				sessions[index].color = session.colorPicker.pastilleColor;
				greenOil.storage.setSessions(sessions);
			});
		},

		loadSession: function(session) {

			let index = this.sessions.indexOf(session);

			this.sessions.forEach((session, i) => {
				// update UI
				if (i === index) session.select();
				else session.unselect();
			});

			// then load session
			popup.storageController.loadSession(index);
		},

		Session: class Session {

			constructor(name, color, select, isAnOldSession) {

				this.isAnOldSession = isAnOldSession; // if the session has just been created ( !isAnOldSession ) it will be added in edit mode
				this.element = Session.TEMPLATE.cloneNode(true); // part of the UI corresponding to this session
				this.inputElement = this.element.getElementsByTagName('input')[0]; // text field used to modify session name
				this.paragraphElement = this.element.getElementsByTagName('p')[0]; // contains the session name
				this.editing = false;
				this.colorBeforeEditing = null;
				this.nameBeforeEditing = null;
				this.selected = false; // true if this session is used, false either
				this.flickeringTimeoutID = null; // timer of a flickering animation that indicate incorrect input

				this.colorPicker = {

					pastille: null, // element that display color chosen by user
					checkMark: null, // visible when session is loaded, its color change in function of the pastille color for a better contrast
					spectrum: null, // div in wich slider can slide (define its amplitude)
					amplitude: NaN,
					sliderBorderWidth: 2,
					slider: null,
					moving: false,

					init: function(session) {

						this.pastille = session.element.getElementsByClassName('user-color')[0];
						this.checkMark = session.element.getElementsByClassName('check-mark')[0];
						this.spectrum = session.element.getElementsByClassName('color-picker')[0];
						this.slider = session.element.getElementsByClassName('color-picker-slider')[0];
						this.slider.ondragstart = () => false;

						this.spectrum.addEventListener('mousedown', (e) => this.mouseDown(e));
						document.addEventListener('mouseup', () => this.mouseUp());
						document.addEventListener('mousemove', (e) => this.mouseMove(e));

						// calculate this.sliderBorderWidth and this.amplitude

						setTimeout(() => {
							const pattern = /^(\d*)px$/;
							const getPixelSize = field => field.match(pattern) ? parseInt(RegExp.$1, 10) : 0;

							const sliderWidth = getPixelSize(getComputedStyle(this.slider).width);
							const spectrumWidth = getPixelSize(getComputedStyle(this.spectrum).width);

							this.amplitude = spectrumWidth - sliderWidth + 2 * this.sliderBorderWidth;
						}, 0);
					},

					setPastilleColor: function(color) {
						this.pastille.style.backgroundColor = color;
					},

					get pastilleColor() {
						return this.pastille.style.backgroundColor;
					},

					mouseDown: function(e) {
						this.moving = true;
						document.body.classList.add('grab');
						this.mouseMove(e);
					},

					mouseUp: function() {
						if (!this.moving) return;
						this.moving = false;
						document.body.classList.remove('grab');
					},

					mouseMove: function(e) {
						if (!this.moving) return;

						let x = e.x - this.spectrum.getBoundingClientRect().left;

						if (x < 0) x = 0;
						else if (x > this.amplitude) x = this.amplitude;

						// update slider position
						this.slider.style.left = x - this.sliderBorderWidth + 'px';

						// update pastille and checkmark color
						this.pastille.style.backgroundColor = popup.utils.colorParser.fracToRgb(x / this.amplitude);
						this.refreshCheckMarkColor();
					},

					// set the slider position according to the pastille color
					refreshSliderPosition: function() {

						// get a fraction (between 0 an 1) from a color
						let position = popup.utils.colorParser.rgbToFrac(this.pastilleColor);
						// then calculate the position
						position *= this.amplitude;

						this.slider.style.left = position - this.sliderBorderWidth + 'px';
					},

					refreshCheckMarkColor: function() {

						// set the checkmark color to black or white in order to make it as visible as posible
						this.checkMark.style.filter = 'brightness(' + popup.utils.colorParser.getMostContrastedColor(this.pastilleColor) + '%)';
					}
				};

				// build element

				this.paragraphElement.textContent = name;

				this.colorPicker.init(this);
				this.colorPicker.setPastilleColor(color || Session.defaultColor);
				this.colorPicker.refreshCheckMarkColor();

				// add event listeners

				this.element.getElementsByClassName('session-content')[0].addEventListener('click', () => {
					// load session when user clicks on it
					if (!Session.editing && !this.selected) popup.sessionsPanelUI.loadSession(this);
				});

				document.addEventListener('keydown', (e) => {
					// allow user to press enter key to validate editing
					if (this.editing && e.keyCode === 13) this.validateEditing();
				});

				// add listeners on option buttons

				let optionButtons = this.element.querySelectorAll('.option');

				for (const btn of optionButtons) {

					let action;

					if (btn.classList.contains('edit')) {
						action = () => {
							if (Session.editing) return;
							this.startEditing();
						};
					} else if (btn.classList.contains('delete')) {
						action = () => {
							if (Session.editing) return;
							this.delete ();
						};
					} else if (btn.classList.contains('cancel')) {
						action = () => this.cancelEditing();
					} else if (btn.classList.contains('validate')) {
						action = () => this.validateEditing();
					}

					btn.addEventListener('click', action);
				}

				// insert to DOM
				popup.sessionsPanelUI.panel.appendChild(this.element);

				// start editing if the session is new
				if (!isAnOldSession) {
					popup.sessionsPanelUI.updateAddButtonVisibility(true);
					this.startEditing();
				}

				// select if it's the current used session
				if (select) this.select();
			}

			get name() {
				return this.paragraphElement.textContent;
			}

			// display the checkmark
			select() {
				this.element.classList.add('selected-session');
				this.selected = true;
			}

			// hide the checkmark
			unselect() {
				this.element.classList.remove('selected-session');
				this.selected = false;
			}

			startEditing() {
				// if the session is new, the color and the name aren't set
				if (this.isAnOldSession) {
					this.colorBeforeEditing = this.colorPicker.pastilleColor;
					this.nameBeforeEditing = this.paragraphElement.innerHTML;
					this.inputElement.value = this.name;
				}

				// sets the cursor position according to the color
				this.colorPicker.refreshSliderPosition();

				this.element.classList.add('editing');
				this.inputElement.focus();

				this.editing = true;
				Session.editing = true;
				popup.sessionsPanelUI.editingStarted();
			}

			cancelEditing() {

				if (!this.editing) return;

				if (this.isAnOldSession) {
					this.colorPicker.setPastilleColor(this.colorBeforeEditing);
					this.colorPicker.refreshCheckMarkColor();
				} else {
					this.delete();
				}

				this.terminateEditing();
			}

			validateEditing() {

				let newName = this.inputElement.value;
				let nameChanged = !this.isAnOldSession || this.name !== newName;
				let colorChanged = !this.isAnOldSession || this.colorBeforeEditing !== this.colorPicker.pastilleColor;

				if (!nameChanged && !colorChanged) this.terminateEditing();

				// check if user as set a name and if this name doesn't already exist
				if (this.inputElement.value === '' || popup.sessionsPanelUI.doesSessionNameExist(this.inputElement.value, this)) {

					// add a flickering effect to let user know that input is incorrect or reset (clear) this effect if it's already running
					if (this.flickeringTimeoutID) clearTimeout(this.flickeringTimeoutID);
					else this.inputElement.classList.add('incorrect-input');

					// remove this effect after 0.75s
					this.flickeringTimeoutID = setTimeout(() => {
						this.inputElement.classList.remove('incorrect-input');
						this.flickeringTimeoutID = null;
					}, 750);

					// refocus input field
					this.inputElement.focus();

					return;
				}

				// set new name
				if (nameChanged) this.paragraphElement.textContent = newName;

				if (this.isAnOldSession) {
					popup.sessionsPanelUI.sessionEdited(this);
				} else {
					popup.sessionsPanelUI.sessionSuccessfulyCreated(this);
					this.isAnOldSession = true;
				}

				this.terminateEditing();
			}

			terminateEditing() {

				// exit editing mode
				this.element.classList.remove('editing');

				this.editing = false;
				Session.editing = false;
				popup.sessionsPanelUI.editingTerminated();
			}

			delete() {
				popup.sessionsPanelUI.panel.removeChild(this.element);
				if (this.isAnOldSession) popup.sessionsPanelUI.sessionDeleted(this);
			}
		},
	},
};

popup.main();
