/*
Copyright 2018, 2019, 2020 Clément Saccoccio
Licenced under GPL-3.0-or-later

This file is part of Iziit.
Iziit is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
Iziit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Iziit. If not, see <https://www.gnu.org/licenses/>.

You can contact us at contact@iziit.org
*/

import greenOil from './common';
import { decode } from 'he';
import { serverAddress, cacheResetsInterval, refreshInterval, maxProductPerRequest, devMode } from '../../iziit-config.sjt';

const greenOilLeclerc = {

	main: function() {

		if (!document.body) return;

		// then get the cache
		greenOil.storage.getAGlobalValue('lastLeclercCacheResetTimeStamp', (timeStamp) => {

			let now = Date.now();

			// and reset it if it's older than cacheResetsInterval
			if (now - timeStamp > cacheResetsInterval) {
				greenOil.storage.setCache('leclerc', {});
				greenOil.storage.setAGlobalValue('lastLeclercCacheResetTimeStamp', now);
			}

			// and finally init the products checker
			this.productsChecker.init();

		}, -1);
	},

	productsChecker: {

		/*

		SOME INFOS ABOUT MAINS VARIABLES (products, elements, ingredientsToAvoid):


		products []
		├── 0 {}
		│   ├── domId (string)
		│   ├── status (string)	// undefined, 'bl' for 'blacklisted', 'wl' for 'whitelisted' or 'dk' for 'don't know'
		│   └── infos {}		// infos that can be send the the server
		│       ├── id (int)
		│       ├── name (string)
		│       └── productPage (string)
		└── etc...

			know the difference:
			products[i].status = undefiend: This product must be checked.
			products[i].status = 'dk': This product has been checked but server don't know if it can be consumed.


		elements {}
		├── sId0 {}	// HTMLLIElement
		├── sId1 {}	// HTMLLIElement
		└── etc...


		ingredientsToAvoid []
		├── 0 (string)	// example: 'palm-oil'
		├── 1 (string)	// example: 'milk'
		└── etc...

		*/

		products: [],
		elements: {},
		ingredientsToAvoid: undefined,
		lastPreferencesUpdateTimestamp: -1,
		pageParser: /Utilitaires\s*\.\s*widget\s*\.\s*initOptions\s*\(\s*'\w*'\s*,\s*((?:(?:(?:(?:\\"|[^"])*(?:[^\\"]|\\"))?"){2})*?(?:\\"|[^"])*)\s*\)\s*;/g,
		shopId: NaN,

		init: function() {

			// get the shop ID

			const match = window.location.href.match(/^https:\/\/[\w-]*\.leclercdrive\.fr\/\w*-(\d*)-/);
			if (!match) return;
			this.shopId = parseInt(match[1], 10);

			// build the all products array except products[i].domId and products[i].status
			this.buildProductsArray();

			setInterval(() => {
				this.refreshElements();            // keeps up-to-date elements array
				this.refreshElementsBgColor();     // keeps up-to-date elements background color
				this.identifyElements();           // bonds elements to products by adding the elements DOM ID in the products objects
				this.refreshIngredientsToAvoid();  // keeps up-to-date ingredientsToAvoid array
				this.checkProducts();              // keeps up-to-date products[i].status by checking if they can be consumed according to the ingredientsToAvoid array
			}, refreshInterval);
		},

		// builds the all products array except products[i].domId and products[i].status
		buildProductsArray: function() {

			let html = document.body.innerHTML;
			let maxIterations = 1000;
			let match;
			let json;
			let rawProductList;

			// This loop is executed as long as the pattern find new matches and as long as maxIterations isn't reached.
			for (let i = 0; i < maxIterations; i++) {

				// The value captured by 'match' corresponds to a JSON object presents inside the script, itself present inside the page HTML.
				match = this.pageParser.exec(html);
				if (!match) break;

				try {
					// Then we parse the captured value.
					json = JSON.parse(match[1]);
				} catch (e) {
					continue;
				}

				// Finally this JSON object is browsed in order to extract the product ID and eventually its name and its page URL.
				// These values will be stored in products[i].infos

				if (json.objProduit && json.objProduit.lstOperationTrade) {

					json.objProduit.lstOperationTrade.forEach((operationTrade) => {

						if (!operationTrade.lstProduits) return;

						operationTrade.lstProduits.forEach((product) => {

							if (!product.iIdProduit || !product.sLibelleLigne1 || !product.sLibelleLigne2) return;

							this.products.push({
								infos: {
									id: product.iIdProduit,
									name: decode(`${product.sLibelleLigne1} ${product.sLibelleLigne2}`).trim(),
									productPage: product.sUrlPageProduit,
								}
							});
						});
					});
				}

				if (json.objContenu && json.objContenu.lstElements) {
					rawProductList = json.objContenu.lstElements;
				} else if (i === maxIterations-1) {
					console.error('maxIterations reached, strange behaviour');
					break;
				} else {
					continue;
				}

				let buildProductsInfosList = (rawProductList) => {

					rawProductList.forEach((product) => {

						if (product.lstEnfants && product.lstEnfants.length !== 0) {
							buildProductsInfosList(product.lstEnfants);
							return;
						}

						if (!product.objElement.iIdProduit || !product.objElement.sLibelleLigne1 || !product.objElement.sLibelleLigne2) return;

						this.products.push({
							infos: {
								id: product.objElement.iIdProduit,
								name: decode(`${product.objElement.sLibelleLigne1} ${product.objElement.sLibelleLigne2}`).trim(),
								productPage: product.objElement.sUrlPageProduit,
							}
						});
					});
				}

				buildProductsInfosList(rawProductList);
			}
		},

		// keeps up-to-date elements array
		refreshElements: function() {

			let freshElements = Array.from(document.getElementsByClassName('liWCRS310_Product'));
			let id;

			freshElements.forEach((freshElement) => {
				id = freshElement.id;
				if (!id) return;
				if (this.elements[id] != freshElement) this.elements[id] = freshElement;
			});
		},

		// keeps up-to-date elements background color
		refreshElementsBgColor: function() {

			for (let product of this.products) {

				if (!product.domId) continue;
				const element = this.elements[product.domId];
				let effectiveStatus;

				if (element.classList.contains('GreenOil-whiteListed')) {
					effectiveStatus = 'wl';
				} else if (element.classList.contains('GreenOil-blackListed')) {
					effectiveStatus = 'bl';
				} else {
					effectiveStatus = 'dk';
				}

				if (effectiveStatus !== product.status) {
					element.classList.remove('GreenOil-whiteListed', 'GreenOil-blackListed');
					if (product.status === 'wl') {
						element.classList.add('GreenOil-whiteListed');
					} else if (product.status === 'bl') {
						element.classList.add('GreenOil-blackListed');
					}
				}
			}
		},

		// bonds elements to products by adding the elements DOM ID in the products objects
		identifyElements: function() {

			let productName;
			let elementName;
			let domId;
			let element;
			let description;
			let link;

			this.products.forEach((product) => {

				// if product is already bound, return
				if (product.domId) return;
				// get the product name...
				productName = product.infos.name;

				// ...in order to check if it matches an element

				for (domId in this.elements) {

					element = this.elements[domId];

					// get the description
					description = element.getElementsByClassName('pWCRS310_Desc')[0];
					if (!description) continue;

					// get the link
					link = description.getElementsByTagName('a')[0];
					if (!link) continue;

					// and then extract it the name
					elementName = link.textContent.trim().replace(/\s+/g, ' ');
					if (elementName === '') continue;

					// in order to check if it correspond to the product name
					if (productName === elementName) {
						product.domId = domId;
						break;
					}
				}
			});
		},

		// keeps up-to-date ingredientsToAvoid array
		refreshIngredientsToAvoid: function() {

			greenOil.storage.getAGlobalValue('currentSessionIndex', (sessionIndex) => {

				greenOil.storage.getSessions((sessions) => {

					let storageModified = false;

					// create the sessions array if it doesn't exist and add a default session in it
					if (!sessions) {
						sessions = [{ name: 'default' }];
						storageModified = true;
					}

					let session = sessions[sessionIndex];
					let preferences = session.preferences;

					// create the preference object if it doesn't exist
					if (!preferences) {
						preferences = {};
						session.preferences = preferences;
						storageModified = true;
					}

					// build a fresh ingredientsToAvoid array

					let newIngredientsToAvoid = [];
					let canEat;

					for (let ingredient in preferences) {
						canEat = preferences[ingredient];
						if (typeof canEat !== 'undefined' && !canEat) newIngredientsToAvoid.push(ingredient);
					}

					// If the fresh array is different from the current one...
					if (!this.ingredientsToAvoid || !greenOil.utils.areArraysEqual(this.ingredientsToAvoid, newIngredientsToAvoid)) {

						// ...update it.
						this.ingredientsToAvoid = newIngredientsToAvoid;
						this.lastPreferencesUpdateTimestamp = Date.now();

						if (this.ingredientsToAvoid.length === 0) {
							// set all products status to whitelisted if the user has no food restriction
							this.products.forEach(product => product.status = 'wl');
						} else {
							// reset all products status
							this.products.forEach(product => product.status = undefined);
						}

						// not necessary but improves elements background refreshing speed
						this.refreshElementsBgColor();
					}

					// save modification
					if (storageModified) greenOil.storage.setSessions(sessions);
				});

			}, 0);
		},

		// keeps up-to-date products[i].status by checking if they can be consumed according to the ingredientsToAvoid array
		checkProducts: function() {

			if (!this.ingredientsToAvoid || this.ingredientsToAvoid.length === 0) return;

			/*

			The cache stores previous data for a period of maximum 2 hours. It prevents the server from being overwhelmed with requests.

			cache {}
			├── '4354827687867' {}	// the hash of ingredientsToAvoid forming a subCache
			│   ├── 12345 (bool)	// indicates if the product with id 12345 can be consumed
			│   └── etc...
			└── etc...

			*/

			// Get the cache,
			greenOil.storage.getCache('leclerc', (cache) => {

				// then the right subCache
				let preferencesHash = this.ingredientsToAvoid.join(' ').hashCode();
				let subCache = cache[preferencesHash];
				let cacheModified = false;

				// and create it if it doesn't exists.
				if (!subCache) {
					subCache = {};
					cache[preferencesHash] = subCache;
					cacheModified = true;
				}

				// Set products[i].status with data found in it.
				
				this.products.forEach((product) => {
					if (devMode) return; // do not use cache when devMode is on
					if (product.status || typeof subCache[product.infos.id] === 'undefined') return;
					product.status = subCache[product.infos.id] ? 'wl' : 'bl';
				});

				// Prepare a payload containing the other products ID that will be sent to server in order to get their status.

				let requestPayload = {
					protocolVersion: 1,
					shopId: this.shopId,
					ingredientsToAvoid: this.ingredientsToAvoid,
					products: [],
				};

				let nbOfAddedProducts = 0;

				for (const product of this.products) {

					// If the product has already a status, check next product.
					if (product.status || !product.domId) continue;

					// set a provisional status until that the server respond
					product.status = 'dk';
					// add the product ID to the payload
					requestPayload.products.push({ id: product.infos.id });

					nbOfAddedProducts++;
					if (nbOfAddedProducts === maxProductPerRequest) break;
				}

				// If payload is empty, don't send it.
				if (requestPayload.products.length === 0) return;

				// sets the product status according to the request and response payload
				let updateProducts = (requestPayload, responsePayload) => {

					let status;

					requestPayload.products.forEach (product => {

						// get status according to the response payload

						if (responsePayload === 'error') {
							status = 'dk';
						} else if (typeof responsePayload.products[product.id] !== 'undefined') {
							status = responsePayload.products[product.id] ? 'wl' : 'bl';
							subCache[product.id] = responsePayload.products[product.id];
							cacheModified = true;
						} else {
							return;
						}

						// get the products corresponding to the ID

						for (let thisProduct of this.products) {
							if (thisProduct.infos.id === product.id) {
								// set the product status
								thisProduct.status = status;
								break;
							}
						}
					});
				};

				// saves the cache if it has been modified
				let saveCache = () => {
					if (cacheModified) greenOil.storage.setCache('leclerc', cache);
				};

				// send the payload
				this.request(requestPayload, (responsePayload) => {

					// set the product status according to the request and response payload
					updateProducts(requestPayload, responsePayload);

					// If the server doesn't expect anything else from the client...
					if (responsePayload.moreInfoNeeded.length === 0) {
						// ...save the cache and return.
						saveCache();
						return;
					}

					// else prepare another request by clearing the products array first
					requestPayload.products = [];

					this.products.forEach((product) => {
						// if the product is part of the moreInfoNeeded products and if more infos are available...
						if (!responsePayload.moreInfoNeeded.includes(product.infos.id) || !product.infos.name || !product.infos.productPage) return;
						// ...add the product and its infos to the payload
						requestPayload.products.push(product.infos);
					});

					// If payload is empty, don't send it.
					if (requestPayload.products.length === 0) return;

					// send the payload
					this.request(requestPayload, (responsePayload) => {
						// set the product status according to the request and response payload, then save the cache
						updateProducts(requestPayload, responsePayload);
						saveCache();
					});
				});
			});
		},

		//////

		// sends the given request payload to the server and retrieves through the callback the response payload or the string 'error'
		request: function(payload, callback) {

			let requestTimeStamp = Date.now();

			greenOil.utils.sendPost(
				`${serverAddress}/api/leclerc/check`,
				JSON.stringify(payload), 
				(statusCode, response) => {
					// if the request is older than the most recent ingredientsToAvoid modification then it's obsolete
					// the execution stop here and no error is fired because it isn't one
					if (this.lastPreferencesUpdateTimestamp > requestTimeStamp) return;
					if (statusCode === 200) {
						// return the response payload
						callback(JSON.parse(response));
					} else {
						console.error('status code: ' + statusCode);
						greenOil.errorMessagesDisplay.showErrorMessage(statusCode);
						callback('error');
					}
			});
		},
	},
};

greenOilLeclerc.main();
