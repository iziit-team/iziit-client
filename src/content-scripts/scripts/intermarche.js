/*
Copyright 2018, 2019, 2020 Clément Saccoccio
Licenced under GPL-3.0-or-later

This file is part of Iziit.
Iziit is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
Iziit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Iziit. If not, see <https://www.gnu.org/licenses/>.

You can contact us at contact@iziit.org
*/

import greenOil from './common';
import { serverAddress, cacheResetsInterval, refreshInterval, maxProductPerRequest, devMode } from '../../iziit-config.sjt';

const greenOilIntermarche = {

	main: function() {

		// then get the cache
		greenOil.storage.getAGlobalValue('lastIntermarcheCacheResetTimeStamp', (timeStamp) => {

			const now = Date.now();

			// and reset it if it's older than cacheResetsInterval
			if (now - timeStamp > cacheResetsInterval) {
				greenOil.storage.setCache('chronodrive', {});
				greenOil.storage.setAGlobalValue('lastChronodriveCacheResetTimeStamp', now);
			}

			// and finally init the products checker
			this.productsChecker.init();

		}, -1);
	},

	productsChecker: {

		/*

		SOME INFOS ABOUT MAINS VARIABLES (products, ingredientsToAvoid):

		products []
		├── 0 {}
		│   ├── id (int)
		│   ├── status (string)    // undefined, 'bl' for 'blacklisted', 'wl' for 'whitelisted' or 'dk' for 'don't know'
		│   └── articles []
		│       ├── {}             // HTMLLElement
		│       └── etc...
		└── etc...

			know the difference:
			products[i].status = undefiend: This product must be checked.
			products[i].status = 'dk': This product has been checked but server don't know if it can be eated.


		ingredientsToAvoid []
		├── 0 (string)	// example: 'palm-oil'
		├── 1 (string)	// example: 'milk'
		└── etc...

		*/

		products: [],
		ingredientsToAvoid: undefined,
		lastPreferencesUpdateTimestamp: -1,
		shop: undefined,

		init: function() {

			const match = window.location.href.match(/^https:\/\/drive\.intermarche\.com\/(.+?)(?:\/.*|$)/);
			if (!match) return;
			this.shop = match[1];

			setInterval(() => {
				this.refreshProductsArray();         // keeps up-to-date products array
				this.refreshArticlesBgColor();       // keeps up-to-date articles background color
				this.refreshIngredientsToAvoid();    // keeps up-to-date ingredientsToAvoid array
				this.checkProducts();                // keeps up-to-date products[i].status by checking if they can be consumed according to the ingredientsToAvoid array
			}, refreshInterval);
		},

		// keeps up-to-date products array by adding to it newly loaded articles
		refreshProductsArray: function() {

			const articles = document.querySelectorAll('.js-vignette_produit .border:not(.GreenOil-detected)');
			let id, status;

			mainLoop: for (const article of articles) {

				id = article.querySelector('[idproduit]');
				if (!id) continue;
				id = parseInt(id.getAttribute('idproduit'), 10);

				for (const product of this.products) {
					if (product.id === id) {
						article.classList.add('GreenOil-detected');
						product.articles.push(article);
						continue mainLoop;
					}
				}

				// set the product status to whitelisted if the user has no food restriction
				status = this.ingredientsToAvoid && this.ingredientsToAvoid.length === 0 ? 'wl' : undefined;

				this.products.push({
					id : id,
					status : status,
					articles : [article],
				});
			}
		},

		// keeps up-to-date articles background color
		refreshArticlesBgColor: function() {

			for (let product of this.products) {
				for (let article of product.articles) {

					let effectiveStatus;

					if (article.classList.contains('GreenOil-whiteListed')) {
						effectiveStatus = 'wl';
					} else if (article.classList.contains('GreenOil-blackListed')) {
						effectiveStatus = 'bl';
					} else {
						effectiveStatus = 'dk';
					}

					if (effectiveStatus !== product.status) {
						article.classList.remove('GreenOil-whiteListed', 'GreenOil-blackListed');
						if (product.status === 'wl') {
							article.classList.add('GreenOil-whiteListed');
						} else if (product.status === 'bl') {
							article.classList.add('GreenOil-blackListed');
						}
					}
				}
			}
		},

		// keeps up-to-date ingredientsToAvoid array
		refreshIngredientsToAvoid: function() {

			greenOil.storage.getAGlobalValue('currentSessionIndex', (sessionIndex) => {
				greenOil.storage.getSessions((sessions) => {

					let storageModified = false;

					// create the sessions array if it doesn't exist and add a default session in it
					if (!sessions) {
						sessions = [{name : 'default'}];
						storageModified = true;
					}

					let session = sessions[sessionIndex];
					let preferences = session.preferences;

					// create the preference object if it doesn't exist
					if (!preferences) {
						preferences = {};
						session.preferences = preferences;
						storageModified = true;
					}

					// build a fresh ingredientsToAvoid array

					let newIngredientsToAvoid = [];
					let canEat;

					for (let ingredient in preferences) {
						canEat = preferences[ingredient];
						if (typeof canEat !== 'undefined' && !canEat) newIngredientsToAvoid.push(ingredient);
					}

					// If the fresh array is different from the current one...
					if (!this.ingredientsToAvoid || !greenOil.utils.areArraysEqual(this.ingredientsToAvoid, newIngredientsToAvoid)) {

						// ...update it.
						this.ingredientsToAvoid = newIngredientsToAvoid;
						this.lastPreferencesUpdateTimestamp = Date.now();

						if (this.ingredientsToAvoid.length === 0) {
							// set all products status to whitelisted if the user has no food restriction
							for (let product of this.products) product.status = 'wl';
						} else {
							// reset all products status
							for (let product of this.products) product.status = undefined;
						}

						// not necessary but improves elements background refreshing speed
						this.refreshArticlesBgColor();
					}

					// save modification
					if (storageModified) greenOil.storage.setSessions(sessions);
				});

			}, 0);
		},

		// keeps up-to-date products[i].status by checking if they can be consumed according to the ingredientsToAvoid array
		checkProducts: function() {

			if (!this.ingredientsToAvoid || this.ingredientsToAvoid.length === 0) return;

			/*

			The cache stores previous data for a period of maximum 2 hours. It prevents the server from being overwhelmed with requests.

			cache {}
			├── '4354827687867' {}	// the hash of ingredientsToAvoid forming a subCache
			│   ├── 12345 (bool)	// indicates if the product with id 12345 can be consumed
			│   └── etc...
			└── etc...

			*/

			// Get the cache,
			greenOil.storage.getCache('intermarche', (cache) => {

				// then the right subCache
				let preferencesHash = this.ingredientsToAvoid.join(' ').hashCode();
				let subCache = cache[preferencesHash];
				let cacheModified = false;

				// and create it if it doesn't exists.
				if (!subCache) {
					subCache = {};
					cache[preferencesHash] = subCache;
					cacheModified = true;
				}

				// Set products[i].status with data found in it.

				for (const product of this.products) {
					if (devMode) break; // do not use cache when devMode is on
					if (product.status || typeof subCache[product.id] === 'undefined') continue;
					product.status = subCache[product.id] ? 'wl' : 'bl';
				}

				// Prepare a payload containing the other products ID that will be sent to server in order to get their status.

				let requestPayload = {
					protocolVersion : 1,
					shop : this.shop,
					ingredientsToAvoid : this.ingredientsToAvoid,
					products : [],
				};

				let nbOfAddedProducts = 0;

				for (const product of this.products) {

					// If the product has already a status, check next product.
					if (product.status) continue;

					// set a provisional status until that the server respond
					product.status = 'dk';
					// add the product ID to the payload
					requestPayload.products.push(product.id);

					nbOfAddedProducts++;
					if (nbOfAddedProducts === maxProductPerRequest) break;
				}

				// If payload is empty, don't send it.
				if (requestPayload.products.length === 0) return;

				// send the payload
				this.request(requestPayload, (responsePayload) => {

					let status;

					for (const id of requestPayload.products) {

						// get status according to the response payload

						if (responsePayload === 'error') {
							status = 'dk';
						} else if (typeof responsePayload[id] === 'boolean') {
							status = responsePayload[id] ? 'wl' : 'bl';
							subCache[id] = responsePayload[id];
							cacheModified = true;
						} else continue;

						// get the products corresponding to the ID

						for (const product of this.products) {
							if (product.id === id) {
								// set the product status
								product.status = status;
								break;
							}
						}
					}

					// save the cache if it has been modified
					if (cacheModified) greenOil.storage.setCache('intermarche', cache);
				});
			});
		},

		//////

		// sends the given request payload to the server and retrieves through the callback the response payload or the string 'error'
		request: function(payload, callback) {

			let requestTimeStamp = Date.now();

			greenOil.utils.sendPost(
				`${serverAddress}/api/intermarche/check`,
				JSON.stringify(payload),
				(statusCode, response) => {
					// if the request is older than the most recent ingredientsToAvoid modification then it's obsolete
					// the execution stop here and no error is throwned because it isn't one
					if (this.lastPreferencesUpdateTimestamp > requestTimeStamp) return;
					if (statusCode === 200) {
						// return the response payload
						callback(JSON.parse(response));
					} else {
						console.error('status code : ' + statusCode);
						greenOil.errorMessagesDisplay.showErrorMessage(statusCode);
						callback('error');
					}
				}
			);
		},
	},
};

greenOilIntermarche.main();
