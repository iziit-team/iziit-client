/*
Copyright 2018, 2019, 2020 Clément Saccoccio
Licenced under GPL-3.0-or-later

This file is part of Iziit.
Iziit is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
Iziit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Iziit. If not, see <https://www.gnu.org/licenses/>.

You can contact us at contact@iziit.org
*/

String.prototype.hashCode = function() {

	let hash = 0;
	if (this.length === 0) return hash;

	for (let i = 0; i < this.length; i++) {
		hash = (hash << 5) - hash + this.charCodeAt(i);
		hash |= 0; // Convert to 32 bits integer
	}

	return hash;
};

String.prototype.capitalizeFirstLetter = function() {
	return this[0].toUpperCase() + this.slice(1);
};

const greenOil = {

	main: function() {
		this.errorMessagesDisplay.init();
	},

	utils: {

		// checks one by one the elements in arrays (non-recursive)
		areArraysEqual: function(array0, array1) {
			if (array0.length !== array1.length) return false;
			for (let i = 0; i < array0.length; i++) {
				if (array0[i] !== array1[i]) return false;
			}
			return true;
		},

		// use the background page to transmit XHR to the server in order to avoid CORB (Cross Origin Read Blocking)
		sendPost: function(url, data, callback) {
			chrome.runtime.sendMessage({
				url: url,
				data: data,
			}, response => callback(response.statusCode, response.body));
		}
	},

	errorMessagesDisplay: {

		maxNbOfDisplayedErrorMessages: 10,
		errorMessagesDisplayingPauseLenght: 600000, // 10 minutes in milliseconds
		errorMessageTemplate: undefined,
		nbOfDisplayedErrorMessages: 0,
		lastErrorMessageTimestamp: NaN,

		init: function() {
			// create the error message template
			this.errorMessageTemplate = document.createElement ('div');
			this.errorMessageTemplate.className = 'GreenOil-error-message';
			this.errorMessageTemplate.innerHTML = '<h1>UNE ERREUR EST SURVENUE</h1><h2>lors de la connection avec le serveur Iziit</h2>';
		},

		// displays an error message about a connection problem baed on the httpErrorCode variable
		showErrorMessage: function(httpErrorCode) {

			// prevents error messages flooding
			if (this.nbOfDisplayedErrorMessages >= this.maxNbOfDisplayedErrorMessages) {
				if (Date.now() - this.lastErrorMessageTimestamp > this.errorMessagesDisplayingPauseLenght) this.nbOfDisplayedErrorMessages = 0;
				else return;
			} else {
				this.nbOfDisplayedErrorMessages++;
				this.lastErrorMessageTimestamp = Date.now();
			}

			// create the custom error message and add it to the DOM

			const p = document.createElement('p');
			p.textContent = 'Code d\'erreur HTTP: ' + httpErrorCode;

			const errorMessage = this.errorMessageTemplate.cloneNode(true);
			errorMessage.appendChild(p);

			document.body.appendChild(errorMessage);

			setTimeout (() => {
				// let the error message fade out
				errorMessage.className += ' GreenOil-vanishing';
			}, 8500);

			setTimeout (() => {
				// remove the error message from DOM when it's invisible
				document.body.removeChild(errorMessage);
			}, 9000);
		},
	},

	storage: {

		/*

		storage architecture:

		├── global {}
		│   ├── currentSessionIndex (int)
		│   └── lastLeclercCacheResetTimeStamp (int)
		├── leclercCache {}
		│   ├── 1008835767 {}
		│   │   ├── 12345 (bool)
		│   │   └── etc...
		│   └── etc...
		└── sessions []
			├── 0
			│   ├── name (str)
			│   ├── color (str)
			│   ├── settings {}
			│   │   └── enable-data-collect (bool)
			│   └── preferences {}
			│       ├── palm-oil (bool)
			│       └── etc...
			└── etc...

		*/

		// set a value that will be stored in 'global' object (cf. 'storage architecture' above)
		setAGlobalValue: function(key, value) {

			chrome.storage.local.get('global', (result) => {

				let globalStorage = result.global;
				if (!globalStorage) globalStorage = {};

				globalStorage[key] = value;

				chrome.storage.local.set({ 'global': globalStorage });
			});
		},

		// set a value that will be stored in 'global' object (cf. 'storage architecture' above)
		getAGlobalValue: function(key, callback, valueToSetIfUndefined) {

			chrome.storage.local.get('global', (result) => {

				let globalStorage = result.global;
				let storageModified = false;

				// create global storage if it doesn't exist
				if (!globalStorage) {
					globalStorage = {};
					storageModified = true;
				}

				if (typeof globalStorage[key] === 'undefined' && typeof valueToSetIfUndefined !== 'undefined') {
					globalStorage[key] = valueToSetIfUndefined;
					storageModified = true;
				}

				// save modifications
				if (storageModified) chrome.storage.local.set({ global: globalStorage });

				callback(globalStorage[key]);
			});
		},

		// retrieves through a callback an array that contains all sessions (cf. 'storage architecture' above)
		// callback (sessions)
		getSessions: function(callback) {
			chrome.storage.local.get('sessions', (result) => callback(result.sessions));
		},

		// replaces in storage the sessions array (cf. 'storage architecture') by the one passed as parameter
		// /!\ the currently used session musn't be altered
		setSessions: function(sessions) {
			chrome.storage.local.set({ sessions: sessions });
		},

		brands: ['leclerc', 'chronodrive', 'auchan', 'intermarche'],

		// retrieves through a callback the cache (cf. 'storage architecture' above)
		// callback (cache)
		getCache: function(brand, callback) {

			if (!this.brands.includes(brand)) return;
			chrome.storage.local.get(brand + 'Cache', (result) => callback(result[brand + 'Cache'] || {}));
		},

		// replaces in storage the cache (cf. 'storage architecture') by the one passed as parameter
		setCache: function(brand, cache) {

			if (!this.brands.includes(brand)) return;

			let cacheContainer = {};
			cacheContainer[brand + 'Cache'] = cache;

			chrome.storage.local.set(cacheContainer);
		},
	},
};

greenOil.main ();

export default greenOil;
