# Iziit

licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)  
website: [iziit.org](https://iziit.org)  
contact: [contact@iziit.org](mailto:contact@iziit.org)

## Development environment

### Prerequisites

Make sure that [Node.js](https://nodejs.org) is installed.  
Even if the addon is client-side, Node.js is used to run commands (see below).

### Setting it up

1. Clone the repository:  
using SSH: `git clone git@gitlab.com:iziit-team/iziit-client.git`  
using HTTPS: `git clone https://gitlab.com/iziit-team/iziit-client.git`

2. Install dependencies:  
`npm install`

### Usage

You have now access to these commands:

- `npm run dev-firefox`  
Builds the addon for Firefox in the `dist` directory and watches for changes.
- `npm run dev-chrome`  
Builds the addon for Chrome in the `dist` directory and watches for changes.
- `npm run build`  
Builds the addon as distributable `zip` files for all supported browser in the `build` directory.
